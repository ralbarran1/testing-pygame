import random

import pygame
import sys

pygame.init()
size = (800, 500)

screen = pygame.display.set_mode(size)

clock = pygame.time.Clock()

DARK_VIOLET = (75, 0, 108)
WHITE = (255, 255, 255)

coord_list = []
for i in range(60):
    x = random.randint(0, 720)
    y = random.randint(0, 420)
    coord_list.append([x, y])

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
    screen.fill(DARK_VIOLET)
    for coord in coord_list:
        x = coord[0]
        y = coord[1]
        pygame.draw.circle(screen, WHITE, (x, y), 2)
        coord[1] += 1
        if coord[1] > 500:
            coord[1] = 0

    pygame.display.flip()
    clock.tick(30)
