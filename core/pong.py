import time

import pygame
import sys

pygame.init()
size = (800, 600)

screen = pygame.display.set_mode(size)

clock = pygame.time.Clock()

ball_x = 400
ball_y = 300
ball_speed_y = 3
ball_speed_x = 3

ball_width = 10
ball_height = 10

player_width = 15
player_height = 90

player_1_x_coord = 50
player_1_y_coord = 300 - 45
player_1_y_speed = 0

player_2_x_coord = 700 - player_width
player_2_y_coord = 300 - 45
player_2_y_speed = 0

BLACK = (0, 0, 0)
GREY = (105, 105, 105)
WHITE = (255, 255, 255)

pygame.mouse.set_visible(True)

player_2_points = 1
player_1_points = 1

game_over = False


def draw_cero_player_1():
    pygame.draw.line(screen, GREY, [300, 100], [300, 10], 20)
    pygame.draw.line(screen, GREY, [260, 100], [260, 10], 20)
    pygame.draw.line(screen, GREY, [260, 19], [300, 19], 20)
    pygame.draw.line(screen, GREY, [260, 100], [300, 19], 20)
    pygame.draw.line(screen, GREY, [270, 90], [295, 90], 20)


def draw_one_player_1():
    pygame.draw.line(screen, GREY, [280, 10], [280, 100], 20)


def draw_two_player_1():
    pygame.draw.line(screen, GREY, [228, 10], [228, 100], 20)


def draw_three_player_1():
    pygame.draw.line(screen, GREY, [176, 10], [176, 100], 20)


# ------------------------------Score of Player 2-----------------------------------------


def draw_cero_player_2():
    # line_right
    pygame.draw.line(screen, GREY, [540, 100], [540, 10], 20)
    # line_up
    pygame.draw.line(screen, GREY, [510, 19], [547, 19], 20)
    # line_diagonal
    pygame.draw.line(screen, GREY, [500, 92], [540, 19], 20)
    # line_left
    pygame.draw.line(screen, GREY, [500, 100], [500, 10], 20)
    # line_down
    pygame.draw.line(screen, GREY, [510, 90], [535, 90], 20)


def draw_one_player_2():
    pygame.draw.line(screen, GREY, [520, 10], [520, 100], 20)


def draw_two_player_2():
    pygame.draw.line(screen, GREY, [572, 10], [572, 100], 20)


def draw_three_player_2():
    pygame.draw.line(screen, GREY, [624, 10], [624, 100], 20)


while not game_over:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            game_over = True

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                player_1_y_speed = -4
            if event.key == pygame.K_DOWN:
                player_1_y_speed = 4

            if event.key == pygame.K_w:
                player_2_y_speed = -4
            if event.key == pygame.K_s:
                player_2_y_speed = 4

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_UP:
                player_1_y_speed = 0
            if event.key == pygame.K_DOWN:
                player_1_y_speed = 0

            if event.key == pygame.K_w:
                player_2_y_speed = 0
            if event.key == pygame.K_s:
                player_2_y_speed = 0

    screen.fill(BLACK)

    if ball_y > 590 or ball_y < 10:
        ball_speed_y *= -1

    # print(pygame.mouse.get_pos())

    if ball_x > 800:
        ball_x = 400
        ball_y = 300
        print("Punto para el Jugador 1, puntos " + str(player_1_points))
        player_1_points = player_1_points + 1

        time.sleep(1.0)

        # ball_speed_x *= -1
        # ball_speed_y *= -1

    if ball_x < 0:
        ball_x = 400
        ball_y = 300
        print("Punto para el Jugador 2, puntos " + str(player_2_points))
        player_2_points = player_2_points + 1

        time.sleep(1.0)

        # ball_speed_x *= -1
        # ball_speed_y *= -1

    if player_1_y_coord >= 520 or player_1_y_coord < 0:
        player_1_y_speed *= -1

    if player_2_y_coord >= 520 or player_2_y_coord < 0:
        player_2_y_speed *= -1

    player_1_y_coord += player_1_y_speed
    player_2_y_coord += player_2_y_speed

    ball_x += ball_speed_x
    ball_y += ball_speed_y

    if player_1_points == 2:
        draw_one_player_1()

    elif player_1_points == 3:
        draw_one_player_1()
        draw_two_player_1()

    elif player_1_points == 4:
        draw_one_player_1()
        draw_two_player_1()
        draw_three_player_1()

        if player_1_points == 4:
            game_over = True

    else:
        draw_cero_player_1()

    if player_2_points == 2:
        draw_one_player_2()

    elif player_2_points == 3:
        draw_one_player_2()
        draw_two_player_2()

    elif player_2_points == 4:
        draw_one_player_2()
        draw_two_player_2()
        draw_three_player_2()
        time.sleep(0.10)
        game_over = True

    else:
        draw_cero_player_2()

    player_1 = pygame.draw.rect(screen, WHITE, (player_1_x_coord, player_1_y_coord, player_width, player_height))
    player_2 = pygame.draw.rect(screen, WHITE, (player_2_x_coord, player_2_y_coord, player_width, player_height))

    background = pygame.draw.line(screen, GREY, [400, 600], [400, 0], 30)
    ball = pygame.draw.circle(screen, WHITE, (ball_x, ball_y), ball_width, ball_height)

    if ball.colliderect(player_1) or ball.colliderect(player_2):
        ball_speed_x *= -1

    pygame.display.flip()

    clock.tick(60)

sys.exit()
